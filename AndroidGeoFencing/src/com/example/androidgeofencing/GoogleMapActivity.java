package com.example.androidgeofencing;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class GoogleMapActivity extends Activity {

	// Google Map
	private GoogleMap googleMap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);
		
		try {
			initilizeMap();
			
			// inside class, for a given lat/lon
			CameraPosition INIT =
			new CameraPosition.Builder()
			.target(new LatLng(23.0300, 72.5800))
			.zoom( 17.5F )
			.bearing( 300F) // orientation
			.tilt( 50F) // viewing angle
			.build();
			 // use GooggleMap mMap to move camera into position
			 googleMap.animateCamera( CameraUpdateFactory.newCameraPosition(INIT) );
			 googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			
			 addMarkerForFence(MainActivity.mUIGeofence1);
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * function to load map If map is not created it will create it for you
	 * */
	@SuppressLint("NewApi")
	private void initilizeMap() {
		if (googleMap == null) {
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

			// check if map is created successfully or not
			if (googleMap == null) {
				Toast.makeText(getApplicationContext(),"Sorry! unable to create maps", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	
	
	
	public  void addMarkerForFence(SimpleGeofence fence){
		if(fence == null){
		    // display en error message and return
		   return;
		}
		googleMap.addMarker( new MarkerOptions()
		  .position( new LatLng(fence.getLatitude(), fence.getLongitude()) )
		  .title("Fence " + fence.getId())
		  .snippet("Radius: " + fence.getRadius()) ).showInfoWindow();
		 
		//Instantiates a new CircleOptions object +  center/radius
		CircleOptions circleOptions = new CircleOptions()
		  .center( new LatLng(fence.getLatitude(), fence.getLongitude()) )
		  .radius( fence.getRadius() )
		  .fillColor(0x40ff0000)
		  .strokeColor(Color.TRANSPARENT)
		  .strokeWidth(2);
		 
		// Get back the mutable Circle
		Circle circle = googleMap.addCircle(circleOptions);
		// more operations on the circle...
		 
		}
}
