package com.learning.helloworld;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class FirstActivity extends Activity implements OnClickListener{

	private View llContainer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_first);
		llContainer = (View) findViewById(R.id.ll_container);
		
		addTextview();
		
		findViewById(R.id.btn_next).setOnClickListener(this);
	}

	
	private void addTextview(){
		TextView tvFirst = new TextView(this);	
		tvFirst.setText("testing");        
		
		TextView tvSecond = new TextView(this);
		tvSecond.setText(getResources().getString(R.string.hello_world));        
		
		LayoutParams param = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		
		tvFirst.setLayoutParams(param);
		tvSecond.setLayoutParams(param);
		
        ((LinearLayout) llContainer).addView(tvFirst);
        ((LinearLayout) llContainer).addView(tvSecond);
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_next:
			Intent i = new Intent(FirstActivity.this, IntentDemoActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivity(i);
			break;

		default:
			break;
		}
	}
}
