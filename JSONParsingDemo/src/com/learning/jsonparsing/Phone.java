package com.learning.jsonparsing;

import com.google.gson.annotations.SerializedName;

public class Phone {

	@SerializedName("mobile")
	private String mobile;
	
	@SerializedName("home")
	private String home;
	
	public String getMobile() {
		return mobile;
	}

	public String getHome() {
		return home;
	}

	public String getOffice() {
		return office;
	}

	@SerializedName("office")
	private String office;
}
