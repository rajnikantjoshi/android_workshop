package com.learning.jsonparsing;

import com.google.gson.annotations.SerializedName;

public class UserJsonResponse {

	@SerializedName("contacts")
	private Contact[] contacts;

	public Contact[] getContacts() {
		return contacts;
	}

}
